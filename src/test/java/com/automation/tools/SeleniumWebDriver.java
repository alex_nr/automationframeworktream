package com.automation.tools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

import static com.automation.tools.Tools.isLinux;

/**
 * Created by tsvetan.karchev on 07/11/2016.
 */
public class SeleniumWebDriver {

    static private ThreadLocal<RemoteWebDriver> webDriver = new ThreadLocal<RemoteWebDriver>() {

        /*
         * initialValue() is called
         */
        @Override
        public synchronized RemoteWebDriver initialValue() {
            //FirefoxProfile profile = new FirefoxProfile();
            //profile.setPreference("network.http.phishy-userpass-length", 255);
//            if (isLinux()) System.setProperty("webdriver.gecko.driver", "");// Chrome localno
//            else System.setProperty("webdriver.chrome.driver", "C:\ChromeDriver/chromedriver.exe");
//            return new ChromeDriver();


            try {
                DesiredCapabilities capability = DesiredCapabilities.firefox();
                return new RemoteWebDriver(new URL("http://192.168.91.54:4000/wd/hub"),
                        capability);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return null;
        }
    };

    static public synchronized WebDriver getWebDriver() {
        return webDriver.get();
    }

    static public synchronized WebDriver setWebDriver() {
//        FirefoxProfile profile = new FirefoxProfile();
//        profile.setPreference("network.http.phishy-userpass-length", 255);
//        if (isLinux()) System.setProperty("webdriver.gecko.driver", "");
//        else System.setProperty("webdriver.chrome.driver", "C:/ChromeDriver/chromedriver.exe");
////        DesiredCapabilities capability = DesiredCapabilities.firefox();
//        WebDriver webDriver2 = new ChromeDriver();
//        webDriver.set(webDriver2);
//        return webDriver2;
        try {
            DesiredCapabilities capability = DesiredCapabilities.firefox();
            RemoteWebDriver webDriver2 = new RemoteWebDriver(new URL("http://192.168.91.54:4000/wd/hub"),
                    capability);
            webDriver.set(webDriver2);
            return webDriver2;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    static public synchronized void close() {
        webDriver.get().quit();
    }



}


