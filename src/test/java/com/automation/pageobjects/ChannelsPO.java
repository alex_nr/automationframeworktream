package com.automation.pageobjects;

import com.automation.tools.Tools;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Divdedov QA on 5/10/2017.
 */
public class ChannelsPO extends Tools{


    static synchronized public WebElement getChamnelsList() {
        return findElementBy(By.className("tag-150-list"));
    }

    static synchronized public WebElement getChamnelLogo() {
        return findElementBy(By.xpath("//*[@id=\"pageContent\"]/a"));

    }

    static synchronized public WebElement getH1TitleChamnelspage() {
        return findElementBy(By.xpath("/html/body/div[1]/div[1]/div[2]/div[1]/h2"));
    }


    static synchronized public WebElement getChannelsBannerFirlstName() {
        return findElementBy(By.xpath("/html/body/div[1]/div[1]/div[1]/div[1]/div[1]/ul/li/div/a"));
    }

    static synchronized public WebElement getChannelsBannerSecondName() {
        return findElementBy(By.xpath("//*[@id=\"pageContent\"]/a/span[2]"));
    }






}


